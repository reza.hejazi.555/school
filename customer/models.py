from django.db import models

class Admin(models.Model):
    Admin_id = models.IntegerField(primary_key=True)
    username = models.CharField(max_length=254, unique=True)
    password = models.CharField(max_length=255, unique=True)
    fullname = models.CharField(max_length=255)

class Categories(models.Model):
    category_id = models.IntegerField(primary_key=True)
    category_name = models.CharField(max_length=50,unique=True)
    category_description = models.TextField()
    category_image = models.ImageField(max_length=200 )

class Customers(models.Model):
    customer_id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=50 )
    last_name = models.CharField(max_length=50 )
    email = models.EmailField(max_length=100,unique=True)
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=200)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    postal_code = models.CharField(max_length=20)

class Orders(models.Model):
    PAYMENT_TYPE_ONLINE = 'O'
    PAYMENT_TYPE_CASH = 'C'
    PAYMENT_TYPE_CHOICES = [
        (PAYMENT_TYPE_CASH, 'Cash'),
        (PAYMENT_TYPE_ONLINE, 'Online'),
    ]
    PAYMENT_STATUS_PENDING = 'P'
    PAYMENT_STATUS_COMPLETE = 'C'
    PAYMENT_STATUS_FAILED = 'F'
    PAYMENT_STATUS_CHOICES = [
        (PAYMENT_STATUS_PENDING, 'Pending'),
        (PAYMENT_STATUS_COMPLETE, 'Complete'),
        (PAYMENT_STATUS_FAILED, 'Failed'),
    ]

    order_id = models.IntegerField(primary_key=True)
    customer_id = models.ForeignKey(Customers,on_delete=models.PROTECT, related_name='related_id')
    order_date = models.DateTimeField(auto_now_add=True )
    total_amount = models.DecimalField(max_digits=10, decimal_places=2 )
    payment_type = models.CharField(
        max_length=1, choices=PAYMENT_TYPE_CHOICES
    )
    status = models.CharField(
        max_length=1, choices=PAYMENT_STATUS_CHOICES, default=PAYMENT_STATUS_PENDING 
        )

class Products(models.Model):
    Product_id = models.IntegerField(primary_key=True)
    Product_name = models.CharField(max_length=50, unique=True)
    description= models.CharField(max_length=200)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(max_length=200 )
    category_id = models.ForeignKey(Categories,on_delete=models.CASCADE, related_name='related_category')

class Ordersdetails(models.Model):
    order_id = models.ForeignKey(Orders, on_delete=models.PROTECT, related_name='related_order')
    product_id = models.IntegerField()
    quantity = models.IntegerField()
    item_notes = models.CharField(max_length=200)
    item_price = models.DecimalField(max_digits=10, decimal_places=2)
    item_discount = models.DecimalField(max_digits=10, decimal_places=2)
    item_total = models.DecimalField(max_digits=10, decimal_places=2)
    item_status = models.CharField(max_length=1)

       
