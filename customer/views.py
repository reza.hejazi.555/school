from django.shortcuts import render
from django.http import HttpResponse
from customer.models import Admin
from customer.models import Categories
from customer.models import Customers
from customer.models import Orders
from customer.models import Products
ok = 'ok'
def all_categories(request):
    q = Categories.objects.all()
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def select_categories(request):
    category_name = request.GET.get('category_name')
    q = Categories.objects.get(category_name=category_name)
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def all_customers(request):
    q = Customers.objects.all()
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def select_customers(request):
    email = request.GET.get('email')
    q = Customers.objects.get(email=email)
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def all_orders(request):
    q = Orders.objects.all()
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def select_orders(request):
    order_id = request.GET.get('order_id')
    q = Orders.objects.get(order_id=order_id)
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def all_products(request):
    q = Products.objects.all()
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def select_products(request):
    Product_name = request.GET.get('Product_name')
    q = Products.objects.get(Product_name=Product_name)
    qs = []
    for cat in q:
        qs.append(cat)
    return HttpResponse(qs)

def insert_categories(request):
    category_name = request.GET.get('category_name')
    category_description = request.GET.get('category_description')
    category_image = request.GET.get('category_image')
    a = Categories(category_name=category_name,category_description=category_description,category_image=category_image)
    a.save()
    return HttpResponse(ok)
# category_name=sard&category_description=ww&category_image=jcdhhc

def insert_customers(request):
    first_name = request.GET.get('first_name')
    last_name = request.GET.get('last_name')
    email = request.GET.get('email')
    phone_number = request.GET.get('phone_number')
    address = request.GET.get('address')
    city = request.GET.get('city')
    state = request.GET.get('state')
    postal_code = request.GET.get('postal_code')
    a = Customers(first_name=first_name,last_name=last_name,email=email,phone_number=phone_number,address=address,city=city,state=state,postal_code=postal_code)
    a.save()
    return HttpResponse(ok)
# first_name=mahdi&last_name=akbari&email=mahdwii.frt@gmail.com&phone_number=090123421346&address=salamat&city=qom&state=jamrasi&postal_code=123456789

def insert_orders(request):
    order_date = request.GET.get('order_date')
    total_amount = request.GET.get('total_amount')
    payment_type = request.GET.get('payment_type')
    customer_id_id = request.GET.get('customer_id')
    a = Orders(order_date=order_date,total_amount=total_amount,payment_type=payment_type,customer_id_id=customer_id_id)
    a.save()
    return HttpResponse(ok)
# order_date=1402/3/27&total_amount=20000&payment_type=o&status=p&customer_id=1

def insert_products(request):
    Product_name = request.GET.get('Product_name')
    description = request.GET.get('description')
    price = request.GET.get('price')
    image = request.GET.get('image')
    category_id_id = request.GET.get('category_id')
    a = Products(Product_name=Product_name,description=description,price=price,image=image,category_id_id=category_id_id)
    a.save()
    return HttpResponse(ok)
# Product_name=pizza&description=pizza%20sard&price=340000&image=aks&category_id=1

def updata_categories(request):
    category_name_old = request.GET.get('category_name_old')
    a = Categories.objects.get(category_name=category_name_old)
    category_name_new = request.GET.get('category_name')
    a.category_name = category_name_new
    category_description = request.GET.get('category_description')
    a.category_description = category_description
    category_image = request.GET.get('category_image')
    a.category_image = category_image
    a.save()
    return HttpResponse(ok)

def updata_customers(request):
    email = request.GET.get('email')
    a = Customers.objects.get(email=email)
    first_name = request.GET.get('first_name')
    a.first_name = first_name
    last_name = request.GET.get('last_name')
    a.last_name = last_name
    phone_number = request.GET.get('phone_number')
    a.phone_number = phone_number
    address = request.GET.get('address')
    a.address = address
    city = request.GET.get('city')
    a.city = city
    state = request.GET.get('state')
    a.state = state
    postal_code = request.GET.get('postal_code')
    a.postal_code = postal_code
    a.save()
    return HttpResponse(ok)

def updata_orders(request):
    order_id = request.GET.get('order_id')
    a = Orders.objects.get(order_id=order_id)
    order_date = request.GET.get('order_date')
    a.order_date = order_date
    total_amount = request.GET.get('total_amount')
    a.total_amount = total_amount
    payment_type = request.GET.get('payment_type')
    a.payment_type = payment_type
    customer_id_id = request.GET.get('customer_id')
    a.customer_id = customer_id_id
    a.save()
    return HttpResponse(ok)

def updata_products(request):
    Product_name_old = request.GET.get('Product_name_old')
    a = Products.objects.get(Product_name=Product_name_old)
    Product_name = request.GET.get('Product_name')
    a.Product_name = Product_name
    description = request.GET.get('description')
    a.description = description
    price = request.GET.get('price')
    a.price = price
    image = request.GET.get('image')
    a.image = image
    category_id_id = request.GET.get('category_id')
    a.category_id = category_id_id
    a.save()
    return HttpResponse(ok)

def Delet_customers(request):
    email = request.GET.get('email')
    a = Customers.objects.get(email=email)
    a.delete()
    return HttpResponse('Deleted')

def Delet_categories(request):
    category_name = request.GET.get('category_name')
    a = Categories.objects.get(category_name=category_name)
    a.delete()
    return HttpResponse('Deleted')

def Delet_products(request):
    Product_name = request.GET.get('Product_name')
    a = Products.objects.get(Product_name=Product_name)
    a.delete()
    return HttpResponse('Deleted')

def Delet_orders(request):
    order_id = request.GET.get('order_id')
    a = Orders.objects.get(order_id=order_id)
    a.delete()
    return HttpResponse('Deleted')

def login_customers(request):
    first_name = request.GET.get('first_name')
    email = request.GET.get('email')
    q = Customers.objects.get(first_name=first_name, email=email)
    q = str(q)
    if len(q)!=0:
        return HttpResponse(ok)
    else:
        return HttpResponse('not ok')

def login_admin(request):
    username = request.GET.get('username')
    password = request.GET.get('password')
    q = Admin.objects.get(username=username,password=password)
    q = str(q)
    if len(q)!=0:
        return HttpResponse(ok)
    else:
        return HttpResponse('not ok')
    
