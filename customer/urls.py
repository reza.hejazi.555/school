from django.urls import path
from . import views

urlpatterns = [
    path('all_categories/', views.all_categories),
    path('select_categories/', views.select_categories),
    path('all_customers/', views.all_customers),
    path('select_customers/', views.select_customers),
    path('all_orders/', views.all_orders),
    path('select_orders/', views.select_orders),
    path('all_products/', views.all_products),
    path('select_products/', views.select_products),
    path('categories/insert/', views.insert_categories),
    path('customers/insert/',views.insert_customers),
    path('orders/insert/',views.insert_orders),
    path('products/insert/',views.insert_products),
    path('updata/categories/', views.updata_categories),
    path('updata/customers/', views.updata_customers),
    path('updata/orders/', views.updata_orders),
    path('updata/products/', views.updata_products),
    path('delete/customers/',views.Delet_customers),
    path('delete/categories/',views.Delet_categories),
    path('delete/products/',views.Delet_products),
    path('delete/orders/',views.Delet_orders),
    path('login_customers/', views.login_customers),
    path('login_admin/', views.login_admin),
]